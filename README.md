# Independent Studies (CMPE 198) #

A compilation of coding projects done during my independent studies course.

### Projects ###

- Secret Sharing
- RSA
- MU Theorem Generator
- Arithmetic Coding