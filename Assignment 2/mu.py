# Eugene Chou
# euchou@ucsc.edu
# Godel's MU theorem generator done in Python

from collections import deque

def theorems(axioms):
	thms = deque()
	thms.append(axioms)
	while thms:
		# apply rules for every node that is dequeued
		nodes = thms.popleft()
		for n in nodes:
			# 1) xI -> xIU
			if n[-1] == "I":
				child = n + "U"
				yield(child, n)
				thms.append([child])

			# 2) Mx -> Mxx
			if n[0] == "M":
				child = n + n[1:]
				yield(child, n)
				thms.append([child])

			# 3) xIIIy -> xUy
			for i in xrange(1, len(n)):
				if n[i:i+3] == "III":
					child = n[0:i] + "U" + n[i+3:]
					yield(child, n)
					thms.append([child])

			# 4) xUUy -> xy	
			k = 0
			while k + 1 < len(n):
				if n[k:k+2] == "UU":
					if k + 2 == len(n) or n[k+2] != "U":
						child = n[0:k] + "" + n[k+2:]
						# print "rule 4"
						yield(child, n)
						thms.append([child])
				k += 1