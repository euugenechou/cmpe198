# Eugene Chou
# euchou@ucsc.edu
# Assignment 0
# CMPE198 - Independent Studies

from fractions import Fraction
import random

def interpolate(points, x) :
# This function takes in a list of order (X, Y) pairs
# Compute the value of the polynomial represented by those points at x. 
# Return that value. 
# Example: interpolate( [(1,2), (3,4)], 0)
	
	y = 0
	n = 0
	origin = 0
	secretSum = 0
	coefficient = 0
	coefficientArr = []

	while (y < len(points)) :
		z = 0
		numerator = 1
		denominator = 1
		while (z < len(points)) : 
			if (z == origin) :
				z += 1
			else :
				numerator *= (x - points[z][0])
				denominator *= (points[origin][0] - points[z][0])
				z += 1
		origin += 1
		coefficient = Fraction(numerator, denominator)
		coefficientArr.append(coefficient)	
		y += 1

	while (n < len(points)) : 
		secretSum += (points[n][1]) * (coefficientArr[n])
		n += 1

	return secretSum
		
def split(val, n, k) :
# Perform an (n, k) secret split, ie k points required to reconstruct the polynomial, n shares distributed. 
# Return the length n list of points (it does not matter where these points are, as long as they are not at x=0). 
# This function IS expected to involve some randomness.
# Example: split(42, 5, 4)
	
	x = 0
	points = n
	variable = 1
	pointList = []
	polynomial = []

	while (x < (k-1)) :
		polynomial.append(random.randint(1, 10))
		x += 1
	polynomial.append(val)

	while (points > 0) : 
		degree = k - 1
		total = 0
		index = 0
		while (degree >= 0) :
			total += (polynomial[index] * (variable**degree))
			index += 1
			degree -= 1
		tempCoord = tuple([variable, total])
		pointList.append(tempCoord)
		variable += 1
		points -= 1

	return pointList
	
# main function for testing purposes
# secret = input("Enter a secret number: ")
# shares = input("Enter number of shares: ")
# pieces = input("Enter number of pieces: ")
# user = input("Enter where to calculate secret at: ")

# print "Sum at %d: %d" % (user, interpolate(split(secret, shares, pieces), user))

