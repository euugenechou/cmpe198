"""
Grading script for asgn0. File to be graded must be named "asgn0",
and must implement the following: interpolate(points, x) and 
split(val, n, k). These functions must work with and return a 
Fraction instance from module fractions.
"""
import random
import asgn0
from fractions import Fraction

def rand(a, b):
    return Fraction(random.random() * (b - a)) + a


def interpolate(points, x):
    val = 0
    for p in points:
        mult = Fraction(1)
        for other in points:
            if other != p:
                mult *= x - other[0]
                mult /= p[0] - other[0]

        val += mult * p[1]

    return val


def split(val, n, k):
    coeffs = [ rand(-10, 10) for i in range(0, k) ]
    coeffs[0] = val
    shares = []
    for i in range(1, n + 1):
        y = Fraction(0)
        for d in range(0, len(coeffs)):
            y += coeffs[d] * i ** d

        shares.append((i, y))

    return shares


if __name__ == '__main__':
    sp = 1
    for i in range(0, 20):
        val = rand(-50, 50)
        n = int(rand(2, 7))
        k = int(rand(2, n))
        p = asgn0.split(val, n, k)
        sample = random.sample(p, k)
        if interpolate(sample, 0) != val:
            print 'Result:   %s' % str(interpolate(sample, 0))
            print 'Expected: %s' % str(val)
            print 'Degree: %d' % (k - 1)
            print 'Interpolated points: %s' % str(sample)
            sp = 0
            break

    print 'Splitting: %d / 1' % sp
    print
    interp = 1
    for i in range(0, 20):
        val = rand(-50, 50)
        n = int(rand(2, 7))
        k = int(rand(2, n))
        p = split(val, n, k)
        sample = random.sample(p, k)
        if asgn0.interpolate(sample, 0) != val:
            print 'Result:   %s' % str(asgn0.interpolate(sample, 0))
            print 'Expected: %s' % str(val)
            print 'Degree: %d' % (k - 1)
            print 'Interpolated points: %s' % str(sample)
            interp = 0
            break

    print 'Interpolation: %d / 1' % interp
    print
    print 'Score: %d / 2' % (sp + interp)