# include <stdio.h>
# include <stdlib.h>
# include "coder.h"
# include "model.h"

int16_t freq[258];
int16_t *total_freq = freq + 1;

void init_model()
{
	for (int16_t i = -1; i <= 256; i++)
	{
		total_freq[i] = i + 1;
	}
	return;
}


void update_model(int symbol)
{
	for (symbol++; symbol <= 256; symbol++)
	{
		total_freq[symbol]++;
	}
	
	if (total_freq[256] == MAX_FREQ)
	{
		for (int i = 0; i <= 256; i++)
		{
			total_freq[i] /= 2;
			if (total_freq[i] <= total_freq[i-1])
			{
				total_freq[i] = total_freq[i-1] + 1;
			}
		}
	}
	return;
}


void get_symbol_scale(SYMBOL *s)
{
	s->scale = total_freq[256];
	return;
}


void int_to_symbol(int c, SYMBOL *s)
{
	s->scale     = total_freq[256];
	s->bot_count = total_freq[c];
	s->top_count = total_freq[c+1];
	return;
}


int symbol_to_int(int count, SYMBOL *s)
{
	int c;
	for (c = 255; count < total_freq[c]; c--);
	s->top_count = total_freq[c+1];
	s->bot_count = total_freq[c];
	return c;
}
