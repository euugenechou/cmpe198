# include <stdio.h>
# include <stdlib.h>
# include "coder.h"
# include "bitio.h"

# define BUFFER_SIZE 256
# define ERROR(x) { fprintf(stderr, "%s\n", x); fflush(stderr); exit(-1); }

static char buffer[BUFFER_SIZE+2]; 		// I/O buffer
static char *current_byte;             	// Pointer for current byte 

static int output_mask;                	// mask to apply to output byte if outputting a 1
static int input_bytes_left;           	// number of inputted bytes left
static int input_bits_left;            	// number of inputted bits left
static int past_eof;                   	// past EOF byte checker

void init_output_stream()
{
	current_byte  = buffer;
	*current_byte = 0;
	output_mask   = 0x80;
	return;
}


void output_bit(FILE *stream, int bit)
{
	if (bit)
	{
		*current_byte |= output_mask;
	}

	output_mask >>= 1;

	if (output_mask == 0)
	{
		output_mask = 0x80;
		current_byte++;
		if (current_byte == buffer + BUFFER_SIZE)
		{
			fwrite(buffer, 1, BUFFER_SIZE, stream);
			current_byte = buffer;
		}
		*current_byte = 0;
	}
	return;
}


void flush_output_stream(FILE *stream)
{
	fwrite(buffer, 1, (size_t) (current_byte - buffer) + 1, stream);
	current_byte = buffer;
	return;
}


void init_input_stream()
{
	input_bits_left  = 0;
	input_bytes_left = 1;
	past_eof = 0;
	return;
}


int16_t input_bit(FILE *stream)
{
	if (input_bits_left == 0)
	{
		current_byte++;
		input_bytes_left--;
		input_bits_left = 8;
		if (input_bytes_left == 0)
		{
			input_bytes_left = fread(buffer, 1, BUFFER_SIZE, stream);
			if (input_bytes_left == 0)
			{
				if (past_eof)
				{
					ERROR("Bad input file\n");
				}
				else
				{
					past_eof = 1;
					input_bytes_left = 2;
				}
			}
			current_byte = buffer;
		}
	}
	input_bits_left--;
	return (*current_byte >> input_bits_left) & 1;
}
