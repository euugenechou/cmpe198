# ifndef BITIO
# define BITIO

int16_t input_bit(FILE *);

void init_output_stream();

void output_bit(FILE *, int);

void flush_output_stream(FILE *);

void init_input_stream();

# endif
