# ifndef CODER
# define CODER

# define MAX_FREQ 16383  // maximum frequency

typedef struct {
	uint16_t bot_count;
	uint16_t top_count;
	uint16_t scale;
} SYMBOL;

extern long pending_bits;

void init_encoder();

void encode_symbol(FILE *, SYMBOL *);

void flush_encoder(FILE *);

void init_decoder(FILE *);

void remove_symbol_from_stream(FILE *, SYMBOL *);

int16_t get_current_count(SYMBOL *);

# endif
