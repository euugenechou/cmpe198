# include <stdio.h>
# include <stdlib.h>
# include "coder.h"
# include "bitio.h"

static uint16_t code;  	// current input code value
static uint16_t bot;   	// bot of the current code range
static uint16_t top;  	// top of the current code range
long pending_bits;      // number of pending bits

void init_encoder()
{
	bot = 0;
	top = 0xffff;
	pending_bits = 0;
	return;
}


void encode_symbol(FILE *stream, SYMBOL *s)
{
	long range = (long) (top - bot) + 1;
	top = bot + (uint16_t) ((range * s->top_count) / s->scale - 1);
	bot = bot + (uint16_t) ((range * s->bot_count ) / s->scale);

	for (;;)
	{
		if ((top & 0x8000) == (bot & 0x8000))
		{
			output_bit(stream, top & 0x8000);
			while (pending_bits > 0)
			{
				output_bit(stream, ~top & 0x8000);
				pending_bits--;
			}
		}

		// case for when numbers are in danger of underflow (i.e 1000... 0111...)
		else if ((bot & 0x4000) && !(top & 0x4000))
		{
			bot &= 0x3fff;
			top |= 0x4000;
			pending_bits += 1;
		}

		else
		{
			return;
		}

		bot <<= 1;
		top <<= 1;
		top  |= 1;
	}
}


void flush_encoder(FILE *stream)
{
	output_bit(stream, bot & 0x4000);
	pending_bits++;
	while (pending_bits-- > 0)
	{
		output_bit(stream, ~bot & 0x4000);
	}
	return;
}


int16_t get_current_count(SYMBOL *s)
{
	long range = (long) (top - bot) + 1;
	int16_t count = (int16_t) ((((long) (code - bot) + 1) * s->scale-1) / range);
	return count;
}


void init_decoder(FILE *stream)
{
	code = 0;

	for (int i = 0; i < 16; i++)
	{
		code <<= 1;
		code += input_bit(stream);
	}

	bot = 0;
	top = 0xffff;
	return;
}


void remove_symbol_from_stream(FILE *stream, SYMBOL *s)
{
	// expand range to account for removal of symbol
	long range = (long) (top - bot) + 1;
	top = bot + (uint16_t) ((range * s->top_count) / s->scale - 1);
	bot = bot + (uint16_t) ((range * s->bot_count) / s->scale);

	// shift out any possible bits
	for (;;)
	{
		// if significant bits match, shift them out
		if ((top & 0x8000) == (bot & 0x8000))
		{
			// does at bottom of function
		}

		// if in danger of underflow (i.e 1000... 0111...) shift out second most significant bit
		else if ((bot & 0x4000) == 0x4000  && (top & 0x4000) == 0)
		{
			code ^= 0x4000;
			bot  &= 0x3fff;
			top  |= 0x4000;
		}

		// return if nothing can be shifted out
		else
		{
			return;
		}

		// shift out bits
		bot  <<= 1;
		top  <<= 1;
		top   |= 1;
		code <<= 1;
		code  += input_bit(stream);
	}
}
