# include <stdio.h>
# include <stdlib.h>
# include <getopt.h>
# include "coder.h"
# include "model.h"
# include "bitio.h"

# define OPTIONS "i:o:ed"
# define ERROR(x) { fprintf(stderr, "%s\n", x); fflush(stderr); exit(1); }

// option flags
int encode = 0; int decode = 0;

// I/O handlers
FILE *input_file  = NULL; char *input  = NULL; 
FILE *output_file = NULL; char *output = NULL;


int main(int argc, char **argv)
{
	// option parser
	int arg = 0;
	while ((arg = getopt(argc, argv, OPTIONS)) != -1)
	{
		switch (arg)
		{
			case 'i': { input  = optarg; break; }
			case 'o': { output = optarg; break; }
			case 'e': { encode = 1;      break; }
			case 'd': { decode = 1;      break; }
		}
	}

	// I/O checker
	if (!input && !output && !encode && !decode)
	{   
		ERROR("No arguments supplied");
	}
	else
	{
		input_file  = fopen(input,  "rb");
		output_file = fopen(output, "wb");
		if (input_file == NULL || output_file == NULL)
		{
			ERROR("Couldn't open one or two of the files");
		}
	}

	// encoder
	if (encode)
	{	
		printf("Compressing %s to %s\n", input, output);

		SYMBOL s;
		int c = 0;

		init_model();
		init_output_stream();
		init_encoder();

		for (;;)
		{
			c = fgetc(input_file);
			int_to_symbol(c, &s);
			encode_symbol(output_file, &s);
			if (c == EOF) { break; }
			update_model(c);
		}

		flush_encoder(output_file);
		flush_output_stream(output_file);
	}

	// decoder
	if (decode)
	{
		printf("Uncompressing %s to %s\n", input, output);

		SYMBOL s;
		int c = 0;
		int count = 0;

		init_model();
		init_input_stream();
		init_decoder(input_file);

		for (;;)
		{
			get_symbol_scale(&s);
			count = get_current_count(&s);
			c = symbol_to_int(count, &s);
			remove_symbol_from_stream(input_file, &s);
			if (c == EOF) { break; }
			fputc((char) c, output_file);
			update_model(c);
		}
	}

	// close files and exit program
	fclose(input_file);
	fclose(output_file);
	exit(0);
}
