# Arithmetic Coding

An implementation of Arithmetic coding in C. 

Arithmetic coding is a common algorithm used in both lossless and lossy data compression algorithms. It is an entropy encoding technique, in which the frequently seen symbols are encoded with fewer bits than lesser seen symbols. 

This specific implementation of Arithmetic coding utilizes a fixed model so that writing the model used to compress the file isn't needed in order to uncompress the compressed file.

## Source code

Contains the following source code and header files:

* `ARcoder.c`  		- Main program for compressing/uncompressing files depending on specified command line options.
* `model.c / model.h` - For initializing/updating the model, as well as converting symbols to integers and vice versa.
* `coder.c / coder.h` - For initializing the encoder/decoder and encoding/decoding symbols.
* `bitio.c / bitio.h` - Handles inputting and outputting bits.
* `Makefile`        - To compile source code.

## Usage

To compile program:

```
make
```

To compress a file:

```
./ARcoder -e -i <file to compress> -o <file to write to>
```

To uncompress a file:

```
./ARcoder -d -i <file to uncompress> -o <file to write to>
```

To clean object files:

```
make clean
```