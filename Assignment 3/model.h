# ifndef MODEL
# define MODEL

void init_model();

void update_model(int);

void int_to_symbol(int, SYMBOL *);

void get_symbol_scale(SYMBOL *);

int symbol_to_int(int, SYMBOL *);

# endif
