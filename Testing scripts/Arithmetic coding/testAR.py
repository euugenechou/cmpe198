import subprocess
import os
import filecmp
import random

f = open("rand.txt",'w')

for x in xrange(1,1000):
    r = random.randint(0,127)
    f.write(chr(r))
f.close()

files = [("triples.txt","this should do somewhat better than huffman"),("mostlyA.txt","This should be a lot better than huffman"), ("Huffman.txt","This should be almost the same as Huffman, or a little worse"),("rand.txt","This should also do about the same as Huffman but will vary, it will likely be larger than the file")]


for pair in files:
    file = pair[0]
    print file
    subprocess.call(["./ARcoder","-e","-i",file, "-o","encoded.txt"])
    subprocess.call(["./ARcoder","-d","-i","encoded.txt", "-o","decoded.txt"])
    ptsize = os.path.getsize(file)
    ecsize = os.path.getsize("encoded.txt")

    if(filecmp.cmp(file,"decoded.txt")):
        print "file was maintained"
    else:
        print "file was garbled somehow"

    subprocess.call(["rm", "H_encoded.txt"])
    subprocess.call(["./encode","-i",file, "-o","H_encoded.txt"])
    H_ecsize = os.path.getsize("H_encoded.txt")

    print "size of original file:", ptsize
    print "size of encoded file:", ecsize, "\n\t", pair[1]
    print "size of huffman file:", H_ecsize, "\n"